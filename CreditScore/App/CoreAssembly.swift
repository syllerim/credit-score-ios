//
//  CoreAssembly.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Moya
import UIKit

final class CoreAssembly {
    
    // MARK: - Assemblies -
    
    private(set) lazy var networkAssembly = NetworkAssembly()
    
    private(set) lazy var creditScoreAssembly = CreditScoreAssembly(networkAssembly: networkAssembly)
    
    // MARK: - Navigation -
    
    private let navigationController: UINavigationController
    
    public init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
}
