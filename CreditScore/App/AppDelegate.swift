//
//  AppDelegate.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 03/12/2019.
//

import UIKit

final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    let appAssembly = AppAssembly()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        appAssembly.setUp(in: window, with: launchOptions)
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }
}
