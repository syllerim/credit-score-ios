//
//  AppAssembly.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import UIKit

class AppAssembly {
    
    private lazy var navigationController = UINavigationController()
    
    private(set) lazy var coreAssembly = CoreAssembly(navigationController: navigationController)
    
    func setUp(in window: UIWindow, with launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        setUpRootWireframe(in: window)
    }
}

extension AppAssembly {
    
    fileprivate func setUpRootWireframe(in window: UIWindow) {
        navigationController.setViewControllers([coreAssembly.creditScoreAssembly.viewController()], animated: true)
        window.rootViewController = navigationController
    }
}
