//
//  AppDelegateUnitTesting.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import UIKit

final class AppDelegateUnitTesting: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = UIViewController()
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }
}
