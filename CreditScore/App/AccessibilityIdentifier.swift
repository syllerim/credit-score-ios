//
//  AccessibilityIdentifier.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 06/12/2019.
//

import Foundation

enum AccessibilityIdentifierItem: String {
    case dashboardTitle = "Dashboard"
    case dashboardMessage
    case dashboardCircle
}
