//
//  AppConfigEnvironment.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Foundation

enum AppConfigEnvironment {
    
    // MARK: - Errors -
    
    private enum Error: Swift.Error {
        case missingPlist
        case missingKey
        case invalidValue
    }
    
    // MARK: - Configuration Keys -
    
    private enum Key: String {
        case baseUrl = "NetworkingBaseURL"
    }
    
    // MARK: - Plist -
    
    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError(Error.missingPlist.localizedDescription)
        }
        return dict
    }()
    
    private static func value<T>(for key: Key) -> T where T: LosslessStringConvertible {
        guard let object = infoDictionary[key.rawValue] else {
            fatalError(Error.missingKey.localizedDescription)
        }
        
        switch object {
        case let value as T:
            return value
        case let string as String:
            guard let value = T(string) else { fallthrough }
            return value
        default:
            fatalError(Error.invalidValue.localizedDescription)
        }
    }
}

extension AppConfigEnvironment {
    
    static let baseUrl = URL(string: value(for: .baseUrl))!
}
