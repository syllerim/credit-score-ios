//
//  main.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import UIKit

private func environmentVariable(with key: String) -> Bool {
    guard let testKey = ProcessInfo.processInfo.environment[key] else { return false }
    return testKey == "true"
}

/// Indicates if the app is executing unit tests.
public let appIsRunningUnitTests = NSClassFromString("XCTestCase") != nil

/// Indicates if the app is the subject of ann automated UI test.
public let appIsRunningUITests = environmentVariable(with: "UI_TESTS")

private let appDelegateClassName = NSStringFromClass(appIsRunningUnitTests ? AppDelegateUnitTesting.self : AppDelegate.self)
_ = UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, appDelegateClassName)
