//
//  FailableDecodable.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Foundation

/// A solution to prevent invalid elements in a decoded array to prevent the entire array from failing decoding.
/// Solution taken from https://stackoverflow.com/a/46369152/2369487.

struct FailableDecodable<Base: Decodable>: Decodable {
    
    let base: Base?
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.base = try? container.decode(Base.self)
    }
}
