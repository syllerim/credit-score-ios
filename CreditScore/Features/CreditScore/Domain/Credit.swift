//
//  Credit.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Foundation

struct Credit: Decodable {
    
    struct CreditReportInfo: Decodable {
        let score: Int
        let maxScoreValue: Int
        let minScoreValue: Int
    }
    
    let creditReportInfo: FailableDecodable<CreditReportInfo>
}

extension Credit {
    
    var scoreValue: Int {
        return creditReportInfo.base?.score ?? 0
    }
    
    var maxScoreValue: Int {
        return creditReportInfo.base?.maxScoreValue ?? 0
    }
    
    var minScoreValue: Int {
        return creditReportInfo.base?.minScoreValue ?? 0
    }
}
