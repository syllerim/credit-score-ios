//
//  CreditScoreInterfaces.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import PromiseKit

protocol CreditScorePresenterProtocol: PresenterProtocol {
}

//sourcery: AutoMockable
protocol CreditScoreViewProtocol: ViewProtocol {
    func showCreditScore(_ credit: Credit)
    func showError(_ error: NetworkLayerError)
}

//sourcery: AutoMockable
protocol CreditScoreRepositoryProtocol {
     /// Obtains the credit data from the repository.
    func getCreditData() -> Promise<Credit>
}
