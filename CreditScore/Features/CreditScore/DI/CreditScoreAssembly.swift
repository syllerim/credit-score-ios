//
//  CreditScoreAssembly.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import UIKit

final class CreditScoreAssembly {
    
    private let networkAssembly: NetworkAssembly
    
    init(networkAssembly: NetworkAssembly) {
        self.networkAssembly = networkAssembly
    }
    
    func viewController() -> CreditScoreViewController {
        let viewController = CreditScoreViewController()
        let repository = CreditScoreRepository(networkLayer: networkAssembly.networkLayer)
        let presenter = CreditScorePresenter(view: viewController, repository: repository)
        viewController.presenter = presenter
        return viewController
    }
}
