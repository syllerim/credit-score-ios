//
//  CreditRepository.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import PromiseKit

final class CreditScoreRepository: CreditScoreRepositoryProtocol {
    
    private let networkLayer: NetworkLayer
    
    init(networkLayer: NetworkLayer) {
        self.networkLayer = networkLayer
    }
    
    func getCreditData() -> Promise<Credit> {
        return firstly {
            networkLayer.requestPromise(.credit)
            .filterSuccessfulStatusCodes()
            .map(Credit.self)
            .mapToError()
        }
    }
}
