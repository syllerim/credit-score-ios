//
//  CreditScorePresenter.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import PromiseKit

final class CreditScorePresenter {
    
    private unowned let view: CreditScoreViewProtocol
    
    private let repository: CreditScoreRepositoryProtocol
    
    init(view: CreditScoreViewProtocol, repository: CreditScoreRepositoryProtocol) {
        self.view = view
        self.repository = repository
    }
}

extension CreditScorePresenter: CreditScorePresenterProtocol {

    func viewDidLoad() {
        fetchData()
    }
    
    func fetchData() {
        firstly {
            repository.getCreditData()
        }.done { [self] data in
            self.view.showCreditScore(data)
        }.catch {
            if let error = $0 as? NetworkLayerError {
                self.view.showError(error)
            }
        }
    }
}
