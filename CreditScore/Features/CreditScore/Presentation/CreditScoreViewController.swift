//
//  CreditScoreViewController.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import UIKit

class CreditScoreViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var circleView: CreditScoreCircleView!
    @IBOutlet weak var scoreDescriptionLabel: UILabel!
    
    // MARK: - Properties
    
    var presenter: CreditScorePresenterProtocol!
    
    // MARK: - Initialization
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Dashboard"
        view.backgroundColor = .white
        circleView.accessibilityIdentifier = AccessibilityIdentifierItem.dashboardCircle.rawValue
        
        presenter.viewDidLoad()
    }
}

extension CreditScoreViewController: CreditScoreViewProtocol {
    
    func showCreditScore(_ credit: Credit) {
        guard credit.maxScoreValue  > 0 else { return }
        
        let text = "Your credit score is \n\(credit.scoreValue )\nout of \(credit.maxScoreValue)"
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 18),
            .foregroundColor: UIColor.black,
            .paragraphStyle: paragraphStyle
        ]

        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        attributedString.addAttributes( [
                       .font: UIFont.systemFont(ofSize: 76),
                       .foregroundColor: UIColor.init(red: CGFloat(0), green: CGFloat(0), blue: CGFloat(0), alpha: 1),
        ], range: NSRange(location: 22, length: String(credit.scoreValue).count))

        scoreDescriptionLabel.attributedText = attributedString
        scoreDescriptionLabel.accessibilityIdentifier = AccessibilityIdentifierItem.dashboardMessage.rawValue
        
        circleView.animateProgress(withValue: Double(credit.scoreValue)/Double(credit.maxScoreValue))
    }
    
    func showError(_ error: NetworkLayerError) {
        presentAlert(for: error)
    }
}
