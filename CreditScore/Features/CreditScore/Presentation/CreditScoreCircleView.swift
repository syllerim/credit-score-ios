//
//  CreditScoreCircleView.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 06/12/2019.
//

import UIKit

@IBDesignable
class CreditScoreCircleView: UIView {
    
    private let shapeLayer = CAShapeLayer()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
}

// MARK: - Animation circle -

extension CreditScoreCircleView {
    
    public func animateProgress(withValue value: Double) {
        let rect = CGRect(x: bounds.origin.x + 10, y: bounds.origin.y + 10, width: bounds.size.width - 20, height: bounds.size.width - 20)
        let circularPath = UIBezierPath(roundedRect: rect, cornerRadius: self.bounds.size.width/2)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.orange.cgColor
        shapeLayer.strokeEnd = 0
        shapeLayer.lineCap = .round
        shapeLayer.lineWidth = 4
        
        layer.addSublayer(shapeLayer)
        
        let strokeAnimation = CreditScoreCircleView.basicShapeStrokeAnimation(toValue: value)
        let colorAnimation = CreditScoreCircleView.basicShapeColorAnimation()
        
        let animations = CAAnimationGroup()
        animations.animations = [strokeAnimation, colorAnimation]
        animations.beginTime = 0
        animations.duration = 2
        animations.fillMode = .both
        animations.isRemovedOnCompletion = false
        shapeLayer.add(animations, forKey: "strokeEndAndColor")
    }
    
    private static func basicShapeStrokeAnimation(toValue value: Double) -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = value
        return animation
    }
    
    private static func basicShapeColorAnimation() -> CABasicAnimation {
        let animation = CABasicAnimation(keyPath: "strokeColor")
        animation.fromValue = UIColor.orange.cgColor
        animation.toValue = UIColor.yellow.cgColor
        return animation
    }
    
    private func configureView() {
        backgroundColor = .clear
        
        let zero = CGFloat(0)
        layer.borderColor = CGColor(srgbRed: zero, green: zero, blue: zero, alpha: 1)
        layer.borderWidth = 2
        layer.cornerRadius = self.frame.width / 2
    }
}
