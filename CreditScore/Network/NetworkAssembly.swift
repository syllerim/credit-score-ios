//
//  NetworkAssembly.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Foundation
import Moya

final class NetworkAssembly {
    
    private(set) lazy var networkLayer: NetworkLayer = {
        #if DEVELOPMENT
        if appIsRunningUITests {
            return NetworkLayer(stubClosure: MoyaProvider.immediatelyStub)
        }
        #endif
        return NetworkLayer()
    }()
}
