//
//  NetworkError+Mapping.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Alamofire
import Moya
import PromiseKit

extension CatchMixin {
    
    /// Converts the promise rejection error to a `NetworkError`.
    func mapToError() -> Promise<T> {
        return recover { error -> Promise<T> in
            throw error.asError
        }
    }
}

extension Error {
    
    var asError: NetworkLayerError {
        if let error = self as? NetworkLayerError {
            return error
        } else if let moyaError = self as? MoyaError {
            switch moyaError {
            case .objectMapping(let self, _):
                return .parsing(self)
            case .statusCode(let response) where response.statusCode == 401:
                return .server
            default:
                return .client(self)
            }
        } else if self is AFError || self is URLError {
            return .network
        } else {
            return .client(self)
        }
    }
}
