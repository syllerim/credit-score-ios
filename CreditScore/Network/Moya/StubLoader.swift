//
//  StubLoader.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Foundation

/// A simple class that loads JSON from disk, based on the supplied file.
class StubLoader {
    
    enum File: String {
        case creditSucess = "credit.success"
    }
    
    /// Constructs a Data object based on the supplied endpoint.
    /// - Parameter file: The file related to the endpoint to tailor the stub data to.
    static func data(for file: File) -> Data {
        return loadJSON(named: file.rawValue)
    }
    
    private static func loadJSON(named name: String) -> Data {
        let bundle = Bundle(for: Self.self)
        guard let url = bundle.url(forResource: name, withExtension: "json", subdirectory: "Stubs") else { return Data() }
        return (try? Data(contentsOf: url)) ?? Data()
    }
}
