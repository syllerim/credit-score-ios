//
//  NetworkLayerError.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Foundation

/// Any error emitted by the RepositoryLayer is of this type.
enum NetworkLayerError: Error {
    
    /// The request was rejected by the server.
    case client(Error)
    
    /// The request failed on the network layer. Either the server is unreachable, or the device does not have a sufficient network connection.
    case network
    
    /// Parsing the response to a model object failed.
    case parsing(Error)
    
    /// The server encountered an internal error and could not process the request.
    case server
}

extension NetworkLayerError: Equatable {
    
    /// This equality ignores associated values.
    static func == (lhs: NetworkLayerError, rhs: NetworkLayerError) -> Bool {
        switch (lhs, rhs) {
        case (.client, .client):
            return true
        case (.network, .network):
            return true
        case (.parsing, .parsing):
            return true
        case (.server, .server):
            return true
        default:
            return false
        }
    }
}
