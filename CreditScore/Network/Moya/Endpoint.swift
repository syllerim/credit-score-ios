//
//  Endpoint.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Foundation
import Moya

typealias NetworkLayer = MoyaProvider<Endpoint>

enum Endpoint {
    case credit
}

extension Endpoint: TargetType {
    var baseURL: URL {
        return AppConfigEnvironment.baseUrl
    }
    
    var path: String {
        switch self {
        case .credit:
            return "mockcredit/values"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .credit:
            return .get
        }
    }
    
    var sampleData: Data {
        switch self {
        case .credit:
            return StubLoader.data(for: StubLoader.File.creditSucess)
        }
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
}

extension Endpoint: AccessTokenAuthorizable {
    
    var authorizationType: AuthorizationType {
        switch self {
        case .credit:
            return .none
        }
    }
}
