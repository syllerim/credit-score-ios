//
//  VIPERProtocols.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

public protocol AssemplyProtocol: class { }

public protocol PresenterProtocol: class {
    func viewDidLoad()
}

extension PresenterProtocol {
    public func viewDidLoad() { }
}

public protocol ViewProtocol: class { }
