//
//  ViewController+Error.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 05/12/2019.
//

import UIKit

extension UIViewController {
    
    /// A generic method to communicate `NetworkLayerError` to the user.
    func presentAlert(for error: NetworkLayerError) {
        let title = error.localizedTitle
        let message = error.localizedMessage
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(.init(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true)
    }
}

extension NetworkLayerError {
    
    var localizedTitle: String {
        switch self {
        case .server:
            return "Something went wrong"
        default:
            return "Connection issues"
        }
    }
    
    var localizedMessage: String {
        switch self {
        case .network:
            return "Check your connection"
        default:
            return "Try again later"
        }
    }
}
