//
//  NetworkLayer+PromiseKit.swift
//  CreditScore
//
//  Created by Mirellys Arteta Davila on 04/12/2019.
//

import Moya
import PromiseKit

extension MoyaProvider {
    
    /// Executes the request and returns a Promise for the response.
    ///
    /// - Parameter target: The endpoint to hit.
    /// - Returns: A promise containing the result of the operation.
    func requestPromise(_ target: Target) -> Promise<Response> {
        return Promise { seal in
            self.request(target) { result in
                switch result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    seal.reject(error)
                }
            }
        }
    }
}

extension Promise where T == Response {
    
    /// Maps received data at key path into a Decodable object. If the conversion fails, the signal errors.
    public func map<D: Decodable>(_ type: D.Type, atKeyPath keyPath: String? = nil, using decoder: JSONDecoder = JSONDecoder(), failsOnEmptyData: Bool = true) -> Promise<D> {
        return map { response in
            try response.map(type, atKeyPath: keyPath, using: decoder, failsOnEmptyData: failsOnEmptyData)
        }
    }
    
    /// Filters out responses where `statusCode` falls within the range 200 - 299.
    public func filterSuccessfulStatusCodes() -> Promise<T> {
        return map { try $0.filterSuccessfulStatusCodes() }
    }
}
