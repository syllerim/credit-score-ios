//
//  CreditScoreUITests.swift
//  CreditScoreUITests
//
//  Created by Mirellys Arteta Davila on 03/12/2019.
//

import XCTest

class CreditScoreUITests: XCTestCase {
    
    private let app = XCUIApplication()
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        app.launchEnvironment = ["UI_TESTS": "true"]
        app.launch()
    }
    
    func testDashboard() {
        XCTAssertTrue(app.existsNavigationBar(withTitle: AccessibilityIdentifierItem.dashboardTitle.rawValue))
        XCTAssertTrue(app.existsText(with: AccessibilityIdentifierItem.dashboardMessage.rawValue))
        XCTAssertTrue(app.elementFor(identifier: AccessibilityIdentifierItem.dashboardCircle.rawValue).exists)
    }
}
