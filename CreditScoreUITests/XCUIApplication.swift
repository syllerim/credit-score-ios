//
//  XCUIApplication.swift
//  CreditScoreUITests
//
//  Created by Mirellys Arteta Davila on 06/12/2019.
//

import XCTest

extension XCUIApplication {
    
    func existsNavigationBar(withTitle string: String) -> Bool {
        return self.navigationBars[string].staticTexts[string].exists
    }
    
    func existsText(with string: String) -> Bool {
        return self.staticTexts[string].exists
    }
    
    private func queryForIdentifier(_ identifier: String) -> XCUIElementQuery {
        return XCUIApplication().descendants(matching: .any).matching(identifier: identifier)
    }
    
    public func elementFor(identifier: String) -> XCUIElement {
        return queryForIdentifier(identifier).firstMatch
    }
}
