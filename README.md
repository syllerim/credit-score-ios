# Credit Score iOS

This is an App with test assessment purpose for ClearScore.

## Architecture

This assesment test follows Clean architecture pattern.  The feature to show the credit score of a user consists of a ViewController, Presenter and Repository.  For this test I didn't use Interactors.

The network layer relies on [Moya](https://github.com/Moya/Moya) considering this framework is really easy to use, it is well tested and with really good adoption in the community, also really easy for Stubbing Data.

Dependency injection ocurrs on the `Assemblies` files, which have the responsibility of the objects creation.

In this example there is no navigation from Dashboard to another screen, but in case it was needed, the resposiblity would be part of `CreditScoreAssembly`.

When running UnitTests the app uses `AppDelegateUnitTesting`.

When running UITests the app uses by default the NetworkLayer Stubbing Data.

### Dependencies

This project uses the following dependencies:

 - [Moya](https://github.com/Moya/Moya) -> Networking
 - [PromiseKit](https://github.com/mxcl/PromiseKit) -> More convenient Asynchronous programming
 - [SnapshotTesting](https://github.com/pointfreeco/swift-snapshot-testing) -> Snapshots of the UIViewControllers on UnitTest target.
 - [SwiftLint](https://github.com/realm/SwiftLint) -> Enforces Swift style and conventions
 - [SwiftyMocky](https://github.com/MakeAWishFoundation/SwiftyMocky) -> Mock Generator to facilitate UnitTest.

### Targets
- Credit Score
- Credit Score Tests
- Credit Score UITests

### Environments
 The project is set up for three different environments, Development, Acceptance, Production. 
 
Under the Directory `Resources` you can find the next files

- Development.xcconfig
- Acceptance.xcconfig
- Production.xcconfig

Those contains the Environment variables in case those needs to be changed, for example, point to different Networking base URL.

### What would I change or improve with more time?
- The proper animations according with the UI sent on the test assesment document.
- More Unit Tests.  For this purpose I mainly testes the CreditScorePresenter and Repository.
- I would add more effort in the Snapshot generated on the `CreditScoreViewControllerTests`, since right now it only shows the circle with the description of the Stubbed values in black, without the colors of the animation.
- I would reconsider using Fastlane/Snapshot if `swift-snapshot-testing` doesn't allow me to screenshot properly the CreditScoreViewController after the animation ocurrs.
- I would be more extrict with the usage of Interactors.
- I would improve the method to show the text `Your credit score is x out of y`.
