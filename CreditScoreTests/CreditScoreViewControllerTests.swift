//
//  CreditScoreViewControllerTests.swift
//  CreditScoreTests
//
//  Created by Mirellys Arteta Davila on 06/12/2019.
//

@testable import CreditScore
import PromiseKit
import SnapshotTesting
import SwiftyMocky
import XCTest

class CreditScoreViewControllerTests: XCTestCase {

    private var credit: Credit!
    private var view: CreditScoreViewProtocolMock!
    private var repository: CreditScoreRepositoryProtocolMock!
    private var presenter: CreditScorePresenter!

    private let precision: Float = 1.0
    
    override func setUp() {
        super.setUp()
    
        credit = CreditScoreTests.loadData(from: StubLoader.File.creditSucess)
        view = CreditScoreViewProtocolMock()
        repository = CreditScoreRepositoryProtocolMock()
        presenter = CreditScorePresenter(view: view, repository: repository)
    }
    
    func testNeutral() {
        // Given the repository returns a non-empty credit object
        Given(repository, .getCreditData(willReturn: .value(credit)))
        
        // When I create the UIViewController and showCreditScore on it
        let viewController = CreditScoreViewController()
        viewController.presenter = presenter
        viewController.viewDidLoad()
        viewController.showCreditScore(credit)
        
        // Then I can assert the image
        assertSnapshots(
            matching: viewController,
            as: [
                .image(on: .iPhone8, precision: precision),
                .image(on: .iPhone8, precision: precision),
            ]
        )
    }
}
