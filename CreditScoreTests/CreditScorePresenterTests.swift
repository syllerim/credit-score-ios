//
//  CreditScoreTests.swift
//  CreditScoreTests
//
//  Created by Mirellys Arteta Davila on 03/12/2019.
//

@testable import CreditScore
import PromiseKit
import SwiftyMocky
import XCTest

class CreditScoreTests: XCTest {

    private var credit: Credit!
    private var view: CreditScoreViewProtocolMock!
    private var repository: CreditScoreRepositoryProtocolMock!
    private var presenter: CreditScorePresenter!

    override func setUp() {
        super.setUp()
        
        PromiseKit.conf.Q.map = nil
        PromiseKit.conf.Q.return = nil
        
        credit = CreditScoreTests.loadData(from: StubLoader.File.creditSucess)
        view = CreditScoreViewProtocolMock()
        repository = CreditScoreRepositoryProtocolMock()
        presenter = CreditScorePresenter(view: view, repository: repository)
    }

    func testGetDataSuccess() {
        // Given the repository return a non-empty credit object
        Given(repository, .getCreditData(willReturn: .value(credit)))

        // When I fetch results
        presenter.fetchData()

        Verify(repository, 1, .getCreditData())
        Verify(view, 1, .showCreditScore(.any))
        Verify(view, 0, .showError(.value(NetworkLayerError.server)))
    }
    
    func testGetDataError() {
        // Given the repository return a error
        Given(repository, .getCreditData(willReturn: .init(error: NetworkLayerError.server)))

        // When I fetch resuts
        presenter.fetchData()
        
        Verify(repository, 1, .getCreditData())
        Verify(view, 0, .showCreditScore(.any))
        Verify(view, 1, .showError(.value(NetworkLayerError.server)))
    }
}

extension CreditScoreTests {
    
    static func loadData(from json: StubLoader.File) -> Credit? {
        let json = StubLoader.data(for: json)
        
        guard let response = try? JSONDecoder().decode(Credit.self, from: json) else {
            XCTFail("Failed to decode stub data.")
            return nil
        }
        return response
    }
}
